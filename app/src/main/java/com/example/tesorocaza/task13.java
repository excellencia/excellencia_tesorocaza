package com.example.tesorocaza;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class task13 extends AppCompatActivity {

    EditText a;
    String z="0";
    int l=1;
    TextView clue;
    ProgressBar progressBar;
    ImageButton imageButton;
    ImageView imageView;
    SharedPreferences sharedPreferences3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        sharedPreferences3 = getSharedPreferences("MyPref3", MODE_PRIVATE);
        if (sharedPreferences3.contains("level"))
        {
            l = sharedPreferences3.getInt("level", 1);
        }

        requestWindowFeature(1);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_task13);
        a = (EditText) findViewById(R.id.key1);
        clue = (TextView) findViewById(R.id.qstn1);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getProgressDrawable().setColorFilter(
                Color.GREEN, android.graphics.PorterDuff.Mode.SRC_IN);
        imageButton = (ImageButton) findViewById(R.id.bar1);
        imageView = (ImageView) findViewById(R.id.pic1);
        imageView.getLayoutParams().height=0;
        imageView.requestLayout();
        setLevel(l);
    }
    public  void read(View view)
    {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setResultDisplayDuration(0);//Text..
        integrator.setScanningRectangle(450, 450);
        integrator.setCameraId(0);
        integrator.initiateScan();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result != null)
        {
            if(result.getContents()==null)
            {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
            else
            {

                a.setText(result.getContents());
                z = a.getText().toString();
                if (l == 3 || l==5) {
                    imageButton.getLayoutParams().height = 0;
                    imageButton.requestLayout();
                } else {
                    imageButton.setVisibility(View.INVISIBLE);
                }
            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
    public  void check(View view)
    {
        z = a.getText().toString();

        if (l == 1)
        {
            if (z.equals(""))
            {
                Toast.makeText(this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
            }
            else if (z.equals(getResources().getString(R.string.task131)))
            {
                l=2;
                AlertDialog alertDialog = new AlertDialog.Builder(task13.this).create();
                alertDialog.setTitle("Level 1 completed");
                alertDialog.setMessage("Best Of Luck \uD83D\uDE0A\uD83D\uDE0A\uD83D\uDC4D\uD83D\uDC4D");
                alertDialog.setCancelable(false);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Level 2", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                       setLevel(2);

                    }
                });

                alertDialog.show();
            }
            else
            {
                Toast.makeText(this, "Wrong key", Toast.LENGTH_SHORT).show();
                imageButton.setVisibility(View.VISIBLE);
                a.setText("");
            }
        }
        else if(l==2)
        {
            if (z.equals(""))
            {
                Toast.makeText(this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
            }
            else if (z.equals(getResources().getString(R.string.task132)))
            {
                l=3;
                AlertDialog alertDialog = new AlertDialog.Builder(task13.this).create();
                alertDialog.setTitle("Level 2 completed");
                alertDialog.setMessage("Best Of Luck \uD83D\uDE0A\uD83D\uDE0A\uD83D\uDC4D\uD83D\uDC4D");
                alertDialog.setCancelable(false);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Level 3", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                       setLevel(3);

                    }
                });

                alertDialog.show();

            }
            else
            {
                Toast.makeText(this, "Wrong key", Toast.LENGTH_SHORT).show();
                imageButton.setVisibility(View.VISIBLE);
                a.setText("");
            }
        }
        else  if(l==3)
        {
            if (z.equals(""))
            {
                Toast.makeText(this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
            }
            else if (z.equals(getResources().getString(R.string.task133)))
            {
                l=4;

                AlertDialog alertDialog = new AlertDialog.Builder(task13.this).create();
                alertDialog.setTitle("Level 3 completed");
                alertDialog.setMessage("Best Of Luck \uD83D\uDE0A\uD83D\uDE0A\uD83D\uDC4D\uD83D\uDC4D");
                alertDialog.setCancelable(false);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Level 4", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                       setLevel(4);

                    }
                });

                alertDialog.show();
            }
            else
            {
                Toast.makeText(this, "Wrong key", Toast.LENGTH_SHORT).show();
                imageButton.getLayoutParams().height= WindowManager.LayoutParams.WRAP_CONTENT;
                imageButton.requestLayout();
                a.setText("");
            }
        }
        else if(l==4)
        {
            if (z.equals(""))
            {
                Toast.makeText(this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
            }
            else if (z.equals(getResources().getString(R.string.task134)))
            {
                l=5;
                AlertDialog alertDialog = new AlertDialog.Builder(task13.this).create();
                alertDialog.setTitle("Level 4 completed");
                alertDialog.setMessage("Best Of Luck \uD83D\uDE0A\uD83D\uDE0A\uD83D\uDC4D\uD83D\uDC4D");
                alertDialog.setCancelable(false);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Level 5", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                       setLevel(5);
                    }
                });

                alertDialog.show();

            }
            else
            {
                Toast.makeText(this, "Wrong key", Toast.LENGTH_SHORT).show();
                imageButton.setVisibility(View.VISIBLE);
                a.setText("");
            }
        }

        else if(l==5)
        {
            if (z.equals(""))
            {
                Toast.makeText(this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
            }
            else if (z.equals(getResources().getString(R.string.keycommon)))
            {
               l=6;
               setLevel(6);

            }
            else
            {
                Toast.makeText(this, "Wrong key", Toast.LENGTH_SHORT).show();
                imageButton.getLayoutParams().height= WindowManager.LayoutParams.WRAP_CONTENT;
                imageButton.requestLayout();
                a.setText("");
            }
        }
    }
    @Override
    public void onBackPressed()
    {
    }
    private void setLevel(int level)
    {
        if(level == 1)
        {
            setTitle("Level 1");
            clue.setText(R.string.q9);
        }
        else if (level == 2)
        {
            a.setText("");
            clue.setText(R.string.q10);
            progressBar.setProgress(20);
            setTitle("Level 2");
            imageButton.setVisibility(View.VISIBLE);
        }
        else if (level == 3)
        {
            a.setText("");
            imageView.getLayoutParams().height= WindowManager.LayoutParams.WRAP_CONTENT;
            imageView.requestLayout();
            clue.setText(R.string.q11);
            progressBar.setProgress(40);
            setTitle("Level 3");
            imageButton.setVisibility(View.VISIBLE);
        }
        else if (level == 4)
        {
            a.setText("");
            imageView.getLayoutParams().height = 0;
            imageView.requestLayout();
            clue.setText(R.string.q12);
            progressBar.setProgress(60);
            setTitle("Level 4");
            imageButton.getLayoutParams().height= WindowManager.LayoutParams.WRAP_CONTENT;
            imageButton.requestLayout();
        }
        else if (level == 5)
        {
            a.setText("");
            clue.setText(R.string.q66);
            progressBar.setProgress(80);
            imageView.getLayoutParams().height= WindowManager.LayoutParams.WRAP_CONTENT;
            imageView.setImageResource(R.drawable.treasure);
            imageView.requestLayout();
            setTitle("Level 5");
            imageButton.setVisibility(View.VISIBLE);
            imageButton.getLayoutParams().height= WindowManager.LayoutParams.WRAP_CONTENT;
            imageButton.requestLayout();

        }
        else if (level == 6)
        {
            Intent intent = new Intent(this,cong.class);
            startActivity(intent);
            finish();
        }
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        SharedPreferences.Editor editor = sharedPreferences3.edit();
        editor.putInt("level", l);
        editor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
