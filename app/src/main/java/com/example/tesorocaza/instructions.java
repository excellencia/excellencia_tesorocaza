package com.example.tesorocaza;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class instructions extends AppCompatActivity {
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_instructions);

    }
    public  void con(View view)
    {
        Toast.makeText(this, "Scan the given QR code", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(instructions.this,base.class);
        startActivity(intent);
        finish();
    }
    @Override
    public void onBackPressed()
    {
    }
}
