package com.example.tesorocaza;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText a, b;
    String name;
    String phone;
    String user1 = "exceteam1",user2="exceteam2",user3="exceteam3",user4="exceteam4";
    String p1 = "exceteam1",p2="exceteam2",p3="exceteam3",p4="exceteam4";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        a = (EditText) findViewById(R.id.editText);
        b = (EditText) findViewById(R.id.editText2);
        Toast.makeText(this, "Login to continue", Toast.LENGTH_SHORT).show();
    }
    public  void submit(View view)
    {
        name = a.getText().toString();
        phone = b.getText().toString();

        if(name.length()>0) {
            if (phone.length() > 0) {
                if (name.equals(user1) && p1.equals(phone) || name.equals(user2) && p2.equals(phone) || name.equals(user3) && p3.equals(phone) || name.equals(user4 )&& p4.equals(phone)) {
                    Intent intent = new Intent(this, instructions.class);
                    startActivity(intent);
                    finish();
                } else
                    Toast.makeText(this, "Incorrect username or password", Toast.LENGTH_SHORT).show();
            }
            else  Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
        }
        else  Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show();

    }
    @Override
    public void onBackPressed()
    {
    }
}
