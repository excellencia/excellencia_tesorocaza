package com.example.tesorocaza;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class base extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 200;
    EditText a;
    String z="0";
    ImageButton imageButton;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_base);
        a = (EditText) findViewById(R.id.editText3);
        imageButton = (ImageButton) findViewById(R.id.imageButton);

        if (checkPermission())
        {

        } else {
            requestPermission();
        }

    }
    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(base.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public void read(View view)
    {

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setResultDisplayDuration(0);//Text..
        integrator.setScanningRectangle(500, 500);
        integrator.setCameraId(0);
        integrator.initiateScan();

    }
    public void next(View view)
    {
        z = a.getText().toString();

        if(z.equals("")) {
            Toast.makeText(this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
        }
        else if(z.equals(getResources().getString( R.string.base1)))
        {
            Intent intent = new Intent(this,task11.class);
            startActivity(intent);
            finish();
        }
        else if(z.equals(getResources().getString(R.string.base2)))
        {

            Intent intent = new Intent(this,task12.class);
            startActivity(intent);
            finish();
        }
        else if(z.equals(getResources().getString(R.string.base3)))
        {

            Intent intent = new Intent(this,task13.class);
            startActivity(intent);
            finish();

        } else if(z.equals(getResources().getString(R.string.base4)))
        {

            Intent intent = new Intent(this,task14.class);
            startActivity(intent);
            finish();
        }
        else
        {
            Toast.makeText(this, "Wrong key", Toast.LENGTH_SHORT).show();
            imageButton.setVisibility(View.VISIBLE);
            a.setText("");
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result != null)
        {
            if(result.getContents()==null)
            {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
            else
            {

                a.setText(result.getContents());
                z = a.getText().toString();
                imageButton.setVisibility(View.INVISIBLE);

            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
    @Override
    public void onBackPressed()
    {
    }

}
